class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper  # Sessions なので注意
end
