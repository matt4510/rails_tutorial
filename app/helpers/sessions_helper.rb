module SessionsHelper
  def log_in(user)
    # これでブラウザ内の一時cookiesに
    # 暗号化済みのユーザーIDを自動的に作成する
    # -> ログイン機能 
    session[:user_id] = user.id
  end

  def remember(user)
    # ユーザーのセッションを永続的にする
    # -> remember機能
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def current_user?(user)
    user == current_user
  end

  # 記憶トークン(cookie)に対応した userオブジェクトを返す
  def current_user
    # user_id は単純に関数内で定義したローカル変数
    if (user_id = session[:user_id]) # session[:user_id]の値があるか判定
      @current_user = User.find_by(id: session[:user_id])
    elsif (user_id = cookies.signed[:user_id]) # cookies.signed[:user_id] があるか判定
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user
      end
    end
  end

  def logged_in?
    # ユーザーがログインしているか確認したい
    !current_user.nil?
  end

  def forget(user)
    # 永続的セッションを破棄する
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def log_out
    # current_user をログアウトする
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # 記憶したURL(もしくはデフォルト値)にリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # アクセスしようとしたURLを覚えておく
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

end
