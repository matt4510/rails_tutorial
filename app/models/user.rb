class User < ApplicationRecord
  attr_accessor :remember_token, :activation_token
  before_save :downcase_email
  before_create :create_activation_digest

  before_save { self.email = self.email.downcase }
  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z0-9\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: true
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  def User.digest(string)
    # 渡された文字列のハッシュ値を返す
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    # ランダムなトークンを作成して返す
    SecureRandom.urlsafe_base64
  end

  def remember
    # 永続セッションのためにユーザーをデータベースに記憶する
    self.remember_token = User.new_token   # 上で定義したやつを書き込む
    hashed_token = User.digest(self.remember_token)
    update_attribute(:remember_digest, hashed_token)
  end

  # 渡されたトークンがダイジェストと一致したら true を返す
  # remember_digest, などと引数で指定して呼び出せるようにした。
  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")    
    # remember_digestがnilの場合、即座にメソッドを終了。
    # -> エラー回避のため　
    return false if remember_digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def forget
    # remember_digestカラムの値をnilに設定する
    # -> remember用のログアウト機能 
    update_attribute(:remember_digest, nil)
  end

  def activate
    update_attribute(:activated, true)
    update_attribute(:activated_at, Time.zone.now)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  @private

    def downcase_email
      self.email = self.email.downcase
    end

    def create_activation_digest
      self.activation_token = User.new_token
      self.activation_digest = User.digest(self.activation_token)
    end
end
